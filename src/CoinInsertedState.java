public class CoinInsertedState implements State {
    private VendingMachineState machine;
    private double currentAmount;

    public CoinInsertedState(VendingMachineState machine, double currentAmount) {
        this.machine = machine;
        this.currentAmount = currentAmount;
    }

    @Override
    public void insertCoin(double amount) {
        this.currentAmount += amount;
    }

    @Override
    public double getCurrentAmount() {
        return currentAmount;
    }

    @Override
    public void printInformation() {
        System.out.println("State: COIN INSERTED, credit: " + currentAmount);
    }

    @Override
    public void selectProduct() {
        double price = machine.getProductPrice();

        if(currentAmount < price) {
            System.out.println("Please insert more coins");
            return;
        }

        currentAmount -= price;
        machine.removeProduct();

        if(machine.getProductQuantity() == 0)
            machine.changeState(new OutOfStockState(machine));
        else if(currentAmount == 0)
            machine.changeState(new AvailableState(machine));
    }

    @Override
    public void ejectCoins() {
        machine.changeState(new AvailableState(machine));
    }
}
