public class Main {

    public static void main(String[] args) {
        VendingMachine machine = new VendingMachineState(2, 1.5);
        MachineController controller = new MachineController(machine);

        controller.runMachine();
    }
}
