public class OutOfStockState implements State {

    private VendingMachineState machine;

    public OutOfStockState(VendingMachineState machine) {

        this.machine = machine;
    }

    @Override
    public void insertCoin(double amount) {
        System.out.println("Sorry, we are out of stock");
    }

    @Override
    public double getCurrentAmount() {
        return 0.0;
    }

    @Override
    public void printInformation() {
        System.out.println("State: OUT OF STOCK");
    }

    @Override
    public void selectProduct() {
        System.out.println("Sorry, we are out of stock");
    }

    @Override
    public void ejectCoins() {
        System.out.println("No coins to eject");
    }
}
