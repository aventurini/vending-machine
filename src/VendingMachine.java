public abstract class VendingMachine {
    public void insertCoin(double amount) {

    }

    public double getCurrentAmount() {
        return 0;
    }

    public void printInformation() {

    }

    public void selectProduct() {

    }

    public void ejectCoins() {

    }

    public void changeState(State state) {

    }

    public void printMessage(String message) {

    }

    public int getProductQuantity() {
        return 0;
    }

    public double getProductPrice() {
        return 0;
    }

    public void removeProduct() {

    }
}
