public interface State {
    void insertCoin(double amount);
    double getCurrentAmount();
    void printInformation();
    void selectProduct();
    void ejectCoins();
}
