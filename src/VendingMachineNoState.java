public class VendingMachineNoState extends VendingMachine {

    private enum State {
        AVAILABLE,
        COIN_INSERTED,
        OUT_OF_STOCK
    };

    private State state;
    private int productQuantity;
    private double productPrice;
    private double credit;

    public VendingMachineNoState(int productQuantity, double productPrice) {
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
    }

    public void insertCoin(double amount) {
        if(state == State.AVAILABLE) {
            state = State.COIN_INSERTED;
            credit = amount;
        }
        else if(state == State.COIN_INSERTED) {
            credit += amount;
        }
        else if(state == State.OUT_OF_STOCK) {
            System.out.println("Sorry, we are out of stock");
        }
    }

    public double getCurrentAmount() {
        return credit;
    }

    public void printInformation() {
        if(state == State.AVAILABLE) {
            System.out.println("State: AVAILABLE");
        }
        else if(state == State.COIN_INSERTED) {
            System.out.println("State: COIN INSERTED, credit: " + credit);
        }
        else if(state == State.OUT_OF_STOCK) {
            System.out.println("State: OUT OF STOCK");
        }

        System.out.println("Available products: " + productQuantity + ", price: " + productPrice);
    }

    public void selectProduct() {
        if(state == State.AVAILABLE) {
            System.out.println("Please insert a coin first");
        }
        else if(state == State.COIN_INSERTED) {
            if(credit < getProductPrice()) {
                System.out.println("Please insert more coins");
                return;
            }

            credit -= getProductPrice();
            removeProduct();

            if(getProductQuantity() == 0)
                state = State.OUT_OF_STOCK;
            else if(credit == 0)
                state = State.AVAILABLE;
        }
    }

    public void ejectCoins() {
        if(state == State.AVAILABLE || state == State.OUT_OF_STOCK) {
            System.out.println("No coins to eject");
        }
        else if(state == State.COIN_INSERTED) {
            credit = 0;
            state = State.AVAILABLE;
        }
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void removeProduct() {
        --productQuantity;
    }
}
