public class VendingMachineState extends VendingMachine {
    private State state;
    private int productQuantity;
    private double productPrice;

    public VendingMachineState(int productQuantity, double productPrice) {
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
        state = new AvailableState(this);
    }

    public void insertCoin(double amount) {
        state.insertCoin(amount);
    }

    public double getCurrentAmount() {
        return state.getCurrentAmount();
    }

    public void printInformation() {
        state.printInformation();
        System.out.println("Available products: " + productQuantity + ", price: " + productPrice);
    }

    public void selectProduct() {
        state.selectProduct();
    }

    public void ejectCoins() {
        state.ejectCoins();
    }

    public void changeState(State state) {
        this.state = state;
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void removeProduct() {
        --productQuantity;
    }
}
