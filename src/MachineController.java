import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MachineController {

    private VendingMachine machine;

    public MachineController(VendingMachine machine) {

        this.machine = machine;
    }

    public void runMachine() {

        while (true) {
            machine.printInformation();
            System.out.println("Actions:\n"
                    + "1) Insert coin\n"
                    + "2) Print current amount\n"
                    + "3) Select product\n"
                    + "4) Eject coin\n"
                    + "5) Exit");

            int action = getIntFromStdin();

            if(action == 5)
                break;

            switch (action) {
                case 1:
                    insertCoin();
                    break;
                case 2:
                    printAmount();
                    break;
                case 3:
                    selectProduct();
                    break;
                case 4:
                    ejectCoins();
                    break;
                default:
                    System.out.println("Unknown action");
            }
        }
    }

    private void ejectCoins() {
        machine.ejectCoins();
    }

    private void selectProduct() {
        machine.selectProduct();
    }

    private void printAmount() {
        System.out.println(machine.getCurrentAmount());
    }

    private void printInfo() {
        machine.printInformation();
    }

    private void insertCoin() {
        System.out.print("Enter amount: ");
        double amount = getDoubleFromStdin();
        machine.insertCoin(amount);
    }

    private int getIntFromStdin() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = "";

        try {
            input = br.readLine();
            return Integer.parseInt(input);
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }

        return -1;
    }

    private double getDoubleFromStdin() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = "";

        try {
            input = br.readLine();
            return Double.parseDouble(input);
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }

        return -1.0;
    }
}
