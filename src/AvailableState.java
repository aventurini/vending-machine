public class AvailableState implements State {

    private VendingMachineState machine;

    public AvailableState(VendingMachineState machine) {
        this.machine = machine;
    }

    @Override
    public void insertCoin(double amount) {
        machine.changeState(new CoinInsertedState(machine, amount));
    }

    @Override
    public double getCurrentAmount() {
        return 0.0;
    }

    @Override
    public void printInformation() {
        System.out.println("State: AVAILABLE");
    }

    @Override
    public void selectProduct() {
        System.out.println("Please insert a coin first");
    }

    @Override
    public void ejectCoins() {
        System.out.println("No coins to eject");
    }
}
